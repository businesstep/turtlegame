﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SmallBasic.Library;

namespace TurtleGame
{
    class Program
    {
        static void Main(string[] args)
        {
            GraphicsWindow.KeyDown += GraphicsWindow_KeyDown;
            Turtle.PenUp();

            GraphicsWindow.BrushColor = "Red";
            var eat = Shapes.AddRectangle(10, 10);

            int x_eat = 200;
            int y_eat = 200;
            Shapes.Move(eat, x_eat, y_eat);
            Random rand = new Random();

            while (true)
            {
                Turtle.Move(10);

                if (Turtle.X >= x_eat && Turtle.X <= x_eat + 10  && Turtle.Y >= y_eat && Turtle.Y <= y_eat + 10)
                {
                    x_eat = rand.Next(0, GraphicsWindow.Width);
                    y_eat = rand.Next(0, GraphicsWindow.Height);

                    Shapes.Move(eat, x_eat, y_eat);

                    Turtle.Speed++;
                }

                if (Turtle.X == 0)
                {
                    Turtle.Angle = 90;
                }

                if (Turtle.Y == 0)
                {
                    Turtle.Angle = 180;
                }
            }
        }

        private static void GraphicsWindow_KeyDown()
        {
            if (GraphicsWindow.LastKey == "Up")
            {
                Turtle.Angle = 0;
            }
            else if (GraphicsWindow.LastKey == "Right")
            {
                Turtle.Angle = 90;            
            }
            else if (GraphicsWindow.LastKey == "Down")
            {
                Turtle.Angle = 180;
            }
            else if (GraphicsWindow.LastKey == "Left")
            {
                Turtle.Angle = 270;
            }
        }
    }
}
